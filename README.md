# uranusjave

Proyecto Cambio Cognitivo Pontificia Universis Javeriana - Departamento de Psicología.

[Install]
apt-get install curl
curl -O https://repo.anaconda.com/archive/Anaconda3-2018.12-Linux-x86_64.sh
bash Anaconda3-2018.12-Linux-x86_64.sh
conda install django
conda install -c chen mod_wsgi
mod_wsgi-express module-config

apt-get install python3
/usr/bin/python3

[superadm]
u fjroldan
pass telAken#

[run]
HTTPS=on python3 manage.py runserver

[Solucion]
https://stackoverflow.com/questions/46759730/django-mod-wsgi-fatal-python-error-py-initialize-no-module-named-encodings

[TAGs]
uranusfront:v1

[apache]
https://tecadmin.net/install-apache-ubuntu1804/
/etc/init.d/apache2 start

ServerAdmin fjroldan09@gmail.com
ServerAlias uranus.javeriana.edu.co

[Cosas]
/root/.bashrc
/root/anaconda3/etc/profile.d/conda.sh
LoadModule wsgi_module "/root/anaconda3/lib/python3.6/site-packages/mod_wsgi/server/mod_wsgi-py36.cpython-36m-x86_64-linux-gnu.so"
WSGIPythonHome "/root/anaconda3"

[tunel]
stunnel4 dev_https
pass: uranus
apt-get purge --auto-remove stunnel4

[apache ssl]
cp ura-ssl.conf /etc/apache2/sites-available/
a2enmod ssl
a2dissite default-ssl.conf
a2ensite ura-ssl.conf 

https://www.digitalocean.com/community/tutorials/how-to-use-apache-http-server-as-reverse-proxy-using-mod_proxy-extension
a2enmod proxy
a2enmod proxy_http

[Conf]

    # Admin email, Server Name (domain name) and any aliases
    ServerAdmin fjroldan09@gmail.com
    ServerAlias uranus.javeriana.edu.co

    Alias /static/ /var/www/app/uranus/task/static
    <Directory /var/www/app/uranus/task/static>
        Require all granted
    </Directory>

    WSGIDaemonProcess uranus python-path=/var/www/app/uranus python-home=/usr
    WSGIProcessGroup uranus
    WSGIScriptAlias / /var/www/app/uranus/uranus/wsgi.py

    <Directory "/var/www/app/uranus/uranus">
        <Files wsgi.py>
            Require all granted
        </Files>
    </Directory>

    ErrorLog /var/www/app/apache/error.log
    CustomLog /var/www/app/apache/access.log combined

    SSLEngine on
    SSLCertificateFile /var/www/app/ssl/server.crt
    SSLCertificateKeyFile /var/www/app/ssl/server.key
    #SSLCACertificateFile /var/www/app/ssl/intermediate.crt

a2enmod ssl
a2enmod rewrite

# Redirect to https
    RewriteEngine On
    RewriteCond %{SERVER_PORT} !^443$
    RewriteRule ^(.*)$ https://%{HTTP_HOST}$1 [R=301,L]



https://pawelzny.com/server/django/2018/02/26/the-most-complete-apache2-config-for-wsgi-django-and-drf/


